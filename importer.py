#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# Rabobank Transactions Importer

import argparse
import json
import logging
import os.path
import sys
from dotenv import load_dotenv

from src.CsvParser import CsvParser
from src.DbConnector import DbConnector
from src.MySqlWorker import MySqlWorker
from src.ReportsWorkerFactory import ReportsWorkerFactory

ENV_PATH = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(ENV_PATH)

from src.settings import *
from src.helpers import *

CONFIG_PATH = os.path.join(os.path.dirname(__file__), 'config')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Rabobank Importer")

    parser.add_argument("--startdate", type=valid_date)
    parser.add_argument("--enddate", type=valid_date)
    parser.add_argument("--report", help="Report Type")
    parser.add_argument("--import-data", help="Perform an import of data")
    
    args = parser.parse_args()
    
    if not args or (not args.report and not args.import_data):
        parser.print_usage()
  
    prepare_logging()
    
    # Generic DB connection
    conn = DbConnector.connection(DB_CONFIG)
    
    # IMPORT PART
    if args.import_data:
        try:
            with open(os.path.join(CONFIG_PATH, 'csv_mapper.json')) as file:
                csv_mapper = json.load(file)
            
            csv = CsvParser(csv_mapper=csv_mapper)
            processed = csv.parse(args.import_data)
            
            logging.info("Ready to import {} records.".format(processed))

            sql = MySqlWorker(connection=conn)
            sql.save(csv.processed, target=TBL_NAME)
        except Exception as e:
            logging.error(e)
            sys.exit(1)

    # REPORTS PART
    if args.report:
        try:
            if not args.enddate or not args.startdate:
                raise argparse.ArgumentTypeError("Start and end date are mandatory (YYYY-MM-DD).")

            factory = ReportsWorkerFactory(connection=conn, config_path=CONFIG_PATH)
            report = factory.produce(args.report)

            report.run(sdate=args.startdate, edate=args.enddate)
            report.print_results()
        except Exception as e:
            logging.error(e)
            sys.exit(1)
