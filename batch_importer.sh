#!/bin/bash

STARTDATE=$1
ENDDATE=$2

if [ $# -eq 0 ]
then
	echo "Pass the start and end date as argumets."
	exit 1
fi

keys=( ALL_DEBIT TRANS_DIRECT ONLY_DEBIT ONLY_CREDIT SU NON-SU DIY IDEAL WMK REGULAR FUEL WITHDRAW TAXES OTHER)
for key in "${keys[@]}"
do
	python importer.py --report=$key --startdate=$STARTDATE --enddate=$ENDDATE
	echo "\n"
done

