# -*- coding: UTF-8 -*-

import os

# in a mysql.connector format
DB_CONFIG = {
    "user": os.environ.get('DB_USER'),
    "password": os.environ.get('DB_PASS'),
    "database": os.environ.get('DB_DBNAME'),
    "host": os.environ.get('DB_HOST'),
    "port": os.environ.get('DB_PORT')
}

# extra configuration for database
TBL_NAME = os.environ.get('DB_TBLNAME')    
