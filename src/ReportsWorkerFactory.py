# -*- coding: UTF-8 -*-
import pkgutil
import importlib

REPORTS_MODULES_PATH = 'src/reports'
REPORTS_MODULE = 'src.reports.'


class ReportsWorkerFactory:
    registered_workers = {}

    def __init__(self, connection, config_path):
        self._conn = connection
        self._cursor = connection.cursor(dictionary=True)
        self._config_path = config_path

        self._register_workers()

    def produce(self, report_type):
        report = self._is_registered(report_type)
        if not report:
            msg = "Unknown type of report [{}]\n".format(report_type)
            msg += "Available reports: {}".format(" ".join(self.registered_workers.keys()))
            raise Exception(msg)

        return report

    def _register_workers(self):
        """Register all reports available"""
        for (module_loader, name, ispkg) in pkgutil.iter_modules([REPORTS_MODULES_PATH]):
            if not name.startswith('Base'):
                module = importlib.import_module(REPORTS_MODULE + name, __package__)
                regclass = getattr(module, name)
                self._register_worker(regclass)

    def _register_worker(self, regclass):
        """Register an individual worker"""
        obj = regclass(self._cursor, self._config_path)
        self.registered_workers[obj.REGNAME] = obj

    def _is_registered(self, report_type):
        """Checks if a given report is registered with this factory"""
        try:
            report = self.registered_workers[report_type]
        except KeyError:
            report = None
        finally:
            return report
