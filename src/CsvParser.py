# -*- coding: UTF-8 -*-

import csv
import logging

from datetime import date

class CsvParser:
    CSV_DELIMITER = ','

    _failed = False
    _processed = []
    
    def __init__(self, csv_mapper={}):
        if not csv_mapper:
            raise Exception("Missing CSV mapper.")
        
        self._csv_mapper = csv_mapper
        
    def parse(self, source_file):
        """Parses the CSV"""
        self._processed = []
        self._failed = False
        
        with open(source_file) as f:
            reader = csv.reader(f, delimiter=self.CSV_DELIMITER)
            next(reader)  # skips the first line
            for row in reader:
                processed_row = self._process_row(row)
                self._processed.append(processed_row)

        # to prevent consecutive logging for the same issue, we do it only once
        if self._failed:
            logging.error(self._failed)
                
        return self.processed.__len__()

    @property
    def processed(self):
        return self._processed
    
    def _process_row(self, row):
        result = {}
      
        try:
            for key, value in self._csv_mapper.items():
                if value != -1:
                    result[key] = row[value]
                else:
                    result[key] = ''
            
            result = self._apply_specials(result)
        except IndexError:
            result[key] = None
            self._failed = "Mapper failed for the combination <{}:{}>".format(key, value)
            
        return result

    def _apply_specials(self, raw_result):
        """Apply special operations on specific fields not covered by the generic mapping"""
        parsed_amount = float(raw_result['amount'].replace(',', '.'))
        
        raw_result['type'] = 'D' if parsed_amount < 0 else 'C'
        
        normalized_amount = parsed_amount if parsed_amount > 0 else parsed_amount * -1
        raw_result['amount'] = str(normalized_amount)
        
        return raw_result