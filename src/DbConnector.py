# -*- coding: UTF-8 -*-

import mysql.connector
import logging
import sys

from mysql.connector import errorcode

class DbConnector:
    @classmethod
    def connection(self, dbconfig):
        try:
            conn = mysql.connector.connect(**dbconfig)
        except mysql.connector.Error as e:
            if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                logging.error("MySQL Connection: Invalid credentials.")
            elif e.errno == errorcode.ER_BAD_DB_ERROR:
                logging.error("MySQL Connection: Inexistent database <{}>.".format(dbconfig['database']))
            sys.exit(1)
            
        return conn
