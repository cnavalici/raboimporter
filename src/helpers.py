# -*- coding: UTF-8 -*-

import logging
from argparse import ArgumentTypeError
from datetime import datetime


def prepare_logging():
    """Batch of commands to prepare the logging"""
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s', "%Y-%m-%d %H:%M:%S")

    # console logging
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)

    # file logging
    fh = logging.FileHandler('importer.log')
    fh.setFormatter(formatter)

    logger.addHandler(ch)
    logger.addHandler(fh)

def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d").isoformat()[:10]
    except ValueError:
        raise ArgumentTypeError("Not a valid date: {}.".format(s))

def validate_dates(sdate, edate):
    """
    Validates two provided dates and put them in order
    :param sdate string YYYY-MM-DD
    :param edate string YYYY-MM-DD
    """
    try:
        sdate = datetime.strptime(sdate, "%Y-%m-%d")
        edate = datetime.strptime(edate, "%Y-%m-%d")

        if sdate > edate:
            sdate, edate = edate, sdate

        return sdate.isoformat()[:10], edate.isoformat()[:10]
    except ValueError:
        raise Exception("One or both provided dates are invalid.")
