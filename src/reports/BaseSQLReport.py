# -*- coding: UTF-8 -*-
import os.path
import json
from abc import ABC, abstractmethod
from datetime import datetime


class BaseSQLReport(ABC):
    __sql_common = """
        SELECT date, CONCAT_WS(' ', name, extra) AS name, ROUND(amount) AS amount
        FROM transactions
        WHERE date BETWEEN CAST(%s AS DATE) AND CAST(%s AS DATE)
        """

    _sql_specific = ''
    _title = ''

    LINE = "-" * 70

    def __init__(self, db_cursor, config_path):
        self._cursor = db_cursor

        self._results = None
        self._interval = ()

    def run(self, sdate, edate):
        """Main run function based on an interval"""
        sql = ("{} {}").format(self.__sql_common, self._sql_specific)

        self._cursor.execute(sql, (sdate, edate))
        results = [ r for r in self._cursor.fetchall() ]

        self._results = sorted(results, key=lambda item: item['date'])

        self._interval = (sdate, edate)

    def print_results(self):
        """Displays the results"""
        if self._results is None:
            raise Exception("Nothing to display. Run first the report using run() method.")

        print("{} [{}/{}]\n".format(self._title, self._interval[0], self._interval[1]))

        print(self.LINE)

        total = 0
        for index, item in enumerate(self._results, start=1):
            name = item['name'][:40]
            print("{:>3d} | {:>10s} | {:>40s} | {:>8.2f}".format(
                index, item['date'].isoformat()[:10], name, item['amount'])
            )
            total += item['amount']

        print(self.LINE)
        print("TOTAL {:>64.2f}".format(total))

    @staticmethod
    def _generate_sql_regex_name(config_path, regex_file):
        with open(os.path.join(config_path, regex_file)) as file:
            regex_names = json.load(file)
            return "AND name REGEXP '{}'".format("|".join(regex_names))
