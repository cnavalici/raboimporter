# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class FuelReport(BaseSQLReport):
    """
    Fuel spendings
    """
    REGNAME = 'FUEL'

    _sql_specific = "AND name LIKE '%SHELL%'"
    _title = "Fuel Expenses"
