# -*- coding: UTF-8 -*-
from .BaseSQLReport import BaseSQLReport


class DIYReport(BaseSQLReport):
    """
    Do-it-Yourself kind of stores
    """
    REGNAME = 'DIY'
    REGEX_FILE = 'diy_regex_names.json'

    _sql_specific = ""
    _title = "Do-it-Yourself (Praxis, Gamma, etc)"

    def __init__(self, db_cursor, config_path):
        self._sql_specific = BaseSQLReport._generate_sql_regex_name(config_path, self.REGEX_FILE)

        super().__init__(db_cursor, config_path)
