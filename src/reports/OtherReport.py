# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class OtherReport(BaseSQLReport):
    """
    Other expenses
    """
    REGNAME = 'OTHER'
    REGEX_FILE = 'other_regex_names.json'

    _sql_specific = ""
    _title = "OTHER Expenses (Not covered by the rest of the reports)"

    def __init__(self, db_cursor, config_path):
        self._sql_specific = BaseSQLReport._generate_sql_regex_name(config_path, self.REGEX_FILE)

        super().__init__(db_cursor, config_path)
