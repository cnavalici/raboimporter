# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class RegularReport(BaseSQLReport):
    """
    Regular Expenses (regular invoices, electricity, internet, phone, hypotheek, car, etc)
    """
    REGNAME = 'REGULAR'

    _sql_specific = "AND rabotype = 'ei'"
    _title = "REGULAR Expenses (gas, licht, hypotheek, car, internet, school, etc)"
