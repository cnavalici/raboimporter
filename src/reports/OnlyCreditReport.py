# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class OnlyCreditReport(BaseSQLReport):
    """
    ALL CREDIT
    """
    REGNAME = 'ONLY_CREDIT'

    _sql_specific = "AND type = 'C'"
    _title = "CREDIT"
