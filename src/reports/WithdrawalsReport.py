# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class WithdrawalsReport(BaseSQLReport):
    """
    All Cash exits
    """
    REGNAME = 'WITHDRAW'

    _sql_specific = "AND rabotype = 'ga'"
    _title = "WITHDRAWALS"
