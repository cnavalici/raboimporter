# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class WmkReport(BaseSQLReport):
    """
    Worstmakerij expenses (Meat Factory)
    """
    REGNAME = 'WMK'

    _sql_specific = "AND name REGEXP 'worstmakerij|worstenmakerij'"
    _title = "Worstmakerij"
