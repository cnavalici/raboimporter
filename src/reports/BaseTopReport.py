# -*- coding: UTF-8 -*-
from abc import ABC, abstractmethod
from datetime import datetime


class BaseTopReport(ABC):
    LINE = "-" * 50
    
    @abstractmethod
    def __init__(self, db_cursor, config_path): pass

    @abstractmethod
    def run(self, sdate, edate):
        """Main run function based on an interval"""

    @abstractmethod
    def print_results(self):
        """Displays the results"""
