# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class NonSupermarketsReport(BaseSQLReport):
    """
    Non-Supermarkets expenses
    """
    REGNAME = 'NON-SU'
    REGEX_FILE = 'non-su_regex_names.json'

    _sql_specific = ""
    _title = "NON-SUPERMARKETS / Other stores (Wibra, Action, Jysk, Zeeman, Kik, De Tuinen, Kwik Fit, etc)"

    def __init__(self, db_cursor, config_path):
        self._sql_specific = BaseSQLReport._generate_sql_regex_name(config_path, self.REGEX_FILE)

        super().__init__(db_cursor, config_path)
