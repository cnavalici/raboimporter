# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class OnlyDebitReport(BaseSQLReport):
    """
    ALL DEBIT excepting transfers, direct payments
    """
    REGNAME = 'ONLY_DEBIT'

    _sql_specific = "AND type = 'D' AND rabotype NOT IN ('tb','bg')"
    _title = "DEBIT"
