# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class TransfersDirectReport(BaseSQLReport):
    """
    Transfers, direct payments, etc ( = ALL_DEBIT - ONLY_DEBIT)
    """
    REGNAME = 'TRANS_DIRECT'

    _sql_specific = "AND type = 'D' AND rabotype IN ('tb','bg')"
    _title = "TRANSFERS and DIRECT PAYMENTS"
