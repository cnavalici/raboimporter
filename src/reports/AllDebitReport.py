# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class AllDebitReport(BaseSQLReport):
    """
    All DEBIT means all the exit money, no matter how/where
    """
    REGNAME = 'ALL_DEBIT'

    _sql_specific = "AND type = 'D'"
    _title = "ALL DEBIT"
