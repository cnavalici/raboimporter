# -*- coding: UTF-8 -*-

import json
import os.path
from collections import defaultdict
from .BaseTopReport import BaseTopReport
from src.helpers import validate_dates


class SupermarketsTop(BaseTopReport):
    REGNAME = 'TOP-SU'
    REGEX_FILE = 'su_regex_names.json'

    _regex_names = []

    def __init__(self, db_cursor, config_path):
        self._cursor = db_cursor

        with open(os.path.join(config_path, self.REGEX_FILE)) as file:
            self._regex_names = json.load(file)

        self._results = []
        self._interval = ()

    def run(self, sdate='', edate=''):
        sdate, edate = validate_dates(sdate, edate)

        sql = """
                 SELECT name, ROUND(SUM(amount),2) AS total FROM transactions
                 WHERE date BETWEEN CAST(%s AS DATE) AND CAST(%s AS DATE)
                 AND name REGEXP %s
                 GROUP BY name
                 ORDER BY total DESC
              """
        regexp = "|".join(self._regex_names)
        self._cursor.execute(sql, (sdate, edate, regexp))

        raw_results = [record for record in self._cursor.fetchall() if record]

        # now we filter the different variations of the names
        results = defaultdict(float)

        for sname in self._regex_names:
            for rec in raw_results:
                if sname.lower() in rec['name'].lower():
                    results[sname] += rec['total']

        self._results = sorted(results.items(), key=lambda item: item[1], reverse=True)
        self._interval = (sdate, edate)

    def print_results(self):
        if not self._results:
            raise Exception("Nothing to display. Run first the report using run() method.")

        print("TOP Supermarkets [{}/{}]\n".format(self._interval[0], self._interval[1]))
        print(self.LINE)

        total = 0
        for index, item in enumerate(self._results, start=1):
            print("{:>2d} | {:>34s} | {:>6.2f}".format(index, item[0], item[1]))
            total += item[1]

        print(self.LINE)
        print("TOTAL {:>42.2f}".format(total))
