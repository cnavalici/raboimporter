# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport

class TaxesReport(BaseSQLReport):
    """
    Various taxes and direct payments (gemeente, dentist, etc)
    """
    REGNAME = 'TAXES'

    _sql_specific = "AND rabotype = 'ac'"
    _title = "TAXES"
