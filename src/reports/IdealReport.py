# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class IdealReport(BaseSQLReport):
    """
    All iDeal Payments
    """
    REGNAME = 'IDEAL'

    _sql_specific = "AND rabotype = 'id'"
    _title = "iDeal Payments"
