# -*- coding: UTF-8 -*-

from .BaseSQLReport import BaseSQLReport


class SupermarketsReport(BaseSQLReport):
    """
    Supermarkets expenses
    """
    REGNAME = 'SU'
    REGEX_FILE = 'su_regex_names.json'

    _sql_specific = ""
    _title = "SUPERMARKETS (Lidl, Aldi, Boni, Kruidvat, Deen, Plus, etc)"

    def __init__(self, db_cursor, config_path):
        self._sql_specific = BaseSQLReport._generate_sql_regex_name(config_path, self.REGEX_FILE)

        super().__init__(db_cursor, config_path)
