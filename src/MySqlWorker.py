# -*- coding: UTF-8 -*-


import logging


class MySqlWorker:
    def __init__(self, connection):
        self._conn = connection

    def save(self, records, target):
        """Saves a bunch of (already) processed records into the target table"""
        try:
            stmt = """INSERT IGNORE INTO {}(rabotype, amount, type, name, extra, date) 
                VALUES(%s, %s, %s, %s, %s, %s)""".format(target)
            
            self._conn.start_transaction()
            
            # prepare the data for the statement
            data = [(r['rabotype'], r['amount'], r['type'], r['name'], r['extra'], r['date']) for r in records ]

            cursor = self._conn.cursor()
            cursor.executemany(stmt, data)
            
            logging.info("Inserted into {}: {} records.".format(target, cursor.rowcount))

            self._conn.commit()
            cursor.close()
        except Exception as e:
            logging.error("Save Failed. {}.".format(e))
            self._conn.rollback()
       